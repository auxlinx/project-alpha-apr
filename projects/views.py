from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from projects.models import Project


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": list_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            user_project = form.save(commit=False)
            user_project.owner = request.user
            user_project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "user_project": form,
    }
    return render(request, "projects/create_receipt.html", context)
