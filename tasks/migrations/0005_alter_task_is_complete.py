# Generated by Django 4.2.7 on 2023-11-06 22:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0004_alter_task_start_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="task",
            name="is_complete",
            field=models.BooleanField(default=False),
        ),
    ]
